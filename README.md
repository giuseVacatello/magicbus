MagicBus is a social-gps for public transport made in Calabria and open data based.

After three years the MagicTeam has decided to open the code, so each city will have its MagicBus.

MagicBus is easy to use, try it on your Android device. 
(https://play.google.com/store/apps/details?id=com.magicbusapp.magicbus&hl=it)